
×U0\Ô  ã               @   s  d  Z  d d l m Z d d l Z d d l Z d d l Td d l Z d Z d d d d	 g Z	 d
 d   Z
 d d d d d d d d d d d 	 Z d d d d d d d d d d d 	 Z d d d d d d d d d d d 	 Z d d d d d d d d d d d d	 
 Z d d   Z d S)aP   Functions measuring similarity using graph edit distance.

The graph edit distance is the number of edge/node changes needed
to make two graphs isomorphic.

The default algorithm/implementation is sub-optimal for some graphs.
The problem of finding the exact Graph Edit Distance (GED) is NP-hard
so it is often slow. If the simple interface `graph_edit_distance`
takes too long for your graph, try `optimize_graph_edit_distance`
and/or `optimize_edit_paths`.

At the same time, I encourage capable people to investigate
alternative GED algorithms, in order to improve the choices available.
é    )Úprint_functionN)Ú*z%Andrey Paramonov <paramon@acdlabs.ru>Úgraph_edit_distanceÚoptimal_edit_pathsÚoptimize_graph_edit_distanceÚoptimize_edit_pathsc              O   s   t  |  |   d  S)N)Úprint)ÚargsÚkwargs© r   úD/tmp/pip-install-8aw2uy0w/networkx/networkx/algorithms/similarity.pyÚdebug_print(   s    r   c             C   sQ   d } xD t  |  | | | | | | | | |	 |
 d  D] \ } } } | } q4 W| S)aÙ  Returns GED (graph edit distance) between graphs G1 and G2.

    Graph edit distance is a graph similarity measure analogous to
    Levenshtein distance for strings.  It is defined as minimum cost
    of edit path (sequence of node and edge edit operations)
    transforming graph G1 to graph isomorphic to G2.

    Parameters
    ----------
    G1, G2: graphs
        The two graphs G1 and G2 must be of the same type.

    node_match : callable
        A function that returns True if node n1 in G1 and n2 in G2
        should be considered equal during matching.

        The function will be called like

           node_match(G1.nodes[n1], G2.nodes[n2]).

        That is, the function will receive the node attribute
        dictionaries for n1 and n2 as inputs.

        Ignored if node_subst_cost is specified.  If neither
        node_match nor node_subst_cost are specified then node
        attributes are not considered.

    edge_match : callable
        A function that returns True if the edge attribute dictionaries
        for the pair of nodes (u1, v1) in G1 and (u2, v2) in G2 should
        be considered equal during matching.

        The function will be called like

           edge_match(G1[u1][v1], G2[u2][v2]).

        That is, the function will receive the edge attribute
        dictionaries of the edges under consideration.

        Ignored if edge_subst_cost is specified.  If neither
        edge_match nor edge_subst_cost are specified then edge
        attributes are not considered.

    node_subst_cost, node_del_cost, node_ins_cost : callable
        Functions that return the costs of node substitution, node
        deletion, and node insertion, respectively.

        The functions will be called like

           node_subst_cost(G1.nodes[n1], G2.nodes[n2]),
           node_del_cost(G1.nodes[n1]),
           node_ins_cost(G2.nodes[n2]).

        That is, the functions will receive the node attribute
        dictionaries as inputs.  The functions are expected to return
        positive numeric values.

        Function node_subst_cost overrides node_match if specified.
        If neither node_match nor node_subst_cost are specified then
        default node substitution cost of 0 is used (node attributes
        are not considered during matching).

        If node_del_cost is not specified then default node deletion
        cost of 1 is used.  If node_ins_cost is not specified then
        default node insertion cost of 1 is used.

    edge_subst_cost, edge_del_cost, edge_ins_cost : callable
        Functions that return the costs of edge substitution, edge
        deletion, and edge insertion, respectively.

        The functions will be called like

           edge_subst_cost(G1[u1][v1], G2[u2][v2]),
           edge_del_cost(G1[u1][v1]),
           edge_ins_cost(G2[u2][v2]).

        That is, the functions will receive the edge attribute
        dictionaries as inputs.  The functions are expected to return
        positive numeric values.

        Function edge_subst_cost overrides edge_match if specified.
        If neither edge_match nor edge_subst_cost are specified then
        default edge substitution cost of 0 is used (edge attributes
        are not considered during matching).

        If edge_del_cost is not specified then default edge deletion
        cost of 1 is used.  If edge_ins_cost is not specified then
        default edge insertion cost of 1 is used.

    upper_bound : numeric
        Maximum edit distance to consider.  Return None if no edit
        distance under or equal to upper_bound exists.

    Examples
    --------
    >>> G1 = nx.cycle_graph(6)
    >>> G2 = nx.wheel_graph(7)
    >>> nx.graph_edit_distance(G1, G2)
    7.0

    See Also
    --------
    optimal_edit_paths, optimize_graph_edit_distance,

    is_isomorphic (test for graph edit distance of 0)

    References
    ----------
    .. [1] Zeina Abu-Aisheh, Romain Raveaux, Jean-Yves Ramel, Patrick
       Martineau. An Exact Graph Edit Distance Algorithm for Solving
       Pattern Recognition Problems. 4th International Conference on
       Pattern Recognition Applications and Methods 2015, Jan 2015,
       Lisbon, Portugal. 2015,
       <10.5220/0005209202710278>. <hal-01168816>
       https://hal.archives-ouvertes.fr/hal-01168816

    NT)r   )ÚG1ÚG2Ú
node_matchÚ
edge_matchÚnode_subst_costÚnode_del_costÚnode_ins_costÚedge_subst_costÚedge_del_costÚedge_ins_costÚupper_boundÚbestcostÚvertex_pathÚ	edge_pathÚcostr   r   r   r   ,   s    {		
c             C   s   t    } d } xx t |  | | | | | | | | |	 |
 d  D]I \ } } } | d k	 rm | | k  rm t    } | j | | f  | } q= W| | f S)aR  Returns all minimum-cost edit paths transforming G1 to G2.

    Graph edit path is a sequence of node and edge edit operations
    transforming graph G1 to graph isomorphic to G2.  Edit operations
    include substitutions, deletions, and insertions.

    Parameters
    ----------
    G1, G2: graphs
        The two graphs G1 and G2 must be of the same type.

    node_match : callable
        A function that returns True if node n1 in G1 and n2 in G2
        should be considered equal during matching.

        The function will be called like

           node_match(G1.nodes[n1], G2.nodes[n2]).

        That is, the function will receive the node attribute
        dictionaries for n1 and n2 as inputs.

        Ignored if node_subst_cost is specified.  If neither
        node_match nor node_subst_cost are specified then node
        attributes are not considered.

    edge_match : callable
        A function that returns True if the edge attribute dictionaries
        for the pair of nodes (u1, v1) in G1 and (u2, v2) in G2 should
        be considered equal during matching.

        The function will be called like

           edge_match(G1[u1][v1], G2[u2][v2]).

        That is, the function will receive the edge attribute
        dictionaries of the edges under consideration.

        Ignored if edge_subst_cost is specified.  If neither
        edge_match nor edge_subst_cost are specified then edge
        attributes are not considered.

    node_subst_cost, node_del_cost, node_ins_cost : callable
        Functions that return the costs of node substitution, node
        deletion, and node insertion, respectively.

        The functions will be called like

           node_subst_cost(G1.nodes[n1], G2.nodes[n2]),
           node_del_cost(G1.nodes[n1]),
           node_ins_cost(G2.nodes[n2]).

        That is, the functions will receive the node attribute
        dictionaries as inputs.  The functions are expected to return
        positive numeric values.

        Function node_subst_cost overrides node_match if specified.
        If neither node_match nor node_subst_cost are specified then
        default node substitution cost of 0 is used (node attributes
        are not considered during matching).

        If node_del_cost is not specified then default node deletion
        cost of 1 is used.  If node_ins_cost is not specified then
        default node insertion cost of 1 is used.

    edge_subst_cost, edge_del_cost, edge_ins_cost : callable
        Functions that return the costs of edge substitution, edge
        deletion, and edge insertion, respectively.

        The functions will be called like

           edge_subst_cost(G1[u1][v1], G2[u2][v2]),
           edge_del_cost(G1[u1][v1]),
           edge_ins_cost(G2[u2][v2]).

        That is, the functions will receive the edge attribute
        dictionaries as inputs.  The functions are expected to return
        positive numeric values.

        Function edge_subst_cost overrides edge_match if specified.
        If neither edge_match nor edge_subst_cost are specified then
        default edge substitution cost of 0 is used (edge attributes
        are not considered during matching).

        If edge_del_cost is not specified then default edge deletion
        cost of 1 is used.  If edge_ins_cost is not specified then
        default edge insertion cost of 1 is used.

    upper_bound : numeric
        Maximum edit distance to consider.

    Returns
    -------
    edit_paths : list of tuples (node_edit_path, edge_edit_path)
        node_edit_path : list of tuples (u, v)
        edge_edit_path : list of tuples ((u1, v1), (u2, v2))

    cost : numeric
        Optimal edit path cost (graph edit distance).

    Examples
    --------
    >>> G1 = nx.cycle_graph(6)
    >>> G2 = nx.wheel_graph(7)
    >>> paths, cost = nx.optimal_edit_paths(G1, G2)
    >>> len(paths)
    84
    >>> cost
    7.0

    See Also
    --------
    graph_edit_distance, optimize_edit_paths

    References
    ----------
    .. [1] Zeina Abu-Aisheh, Romain Raveaux, Jean-Yves Ramel, Patrick
       Martineau. An Exact Graph Edit Distance Algorithm for Solving
       Pattern Recognition Problems. 4th International Conference on
       Pattern Recognition Applications and Methods 2015, Jan 2015,
       Lisbon, Portugal. 2015,
       <10.5220/0005209202710278>. <hal-01168816>
       https://hal.archives-ouvertes.fr/hal-01168816

    NF)Úlistr   Úappend)r   r   r   r   r   r   r   r   r   r   r   Úpathsr   r   r   r   r   r   r   r   ²   s    				
c             c   sJ   xC t  |  | | | | | | | | |	 |
 d  D] \ } } } | Vq. Wd S)aý  Returns consecutive approximations of GED (graph edit distance)
    between graphs G1 and G2.

    Graph edit distance is a graph similarity measure analogous to
    Levenshtein distance for strings.  It is defined as minimum cost
    of edit path (sequence of node and edge edit operations)
    transforming graph G1 to graph isomorphic to G2.

    Parameters
    ----------
    G1, G2: graphs
        The two graphs G1 and G2 must be of the same type.

    node_match : callable
        A function that returns True if node n1 in G1 and n2 in G2
        should be considered equal during matching.

        The function will be called like

           node_match(G1.nodes[n1], G2.nodes[n2]).

        That is, the function will receive the node attribute
        dictionaries for n1 and n2 as inputs.

        Ignored if node_subst_cost is specified.  If neither
        node_match nor node_subst_cost are specified then node
        attributes are not considered.

    edge_match : callable
        A function that returns True if the edge attribute dictionaries
        for the pair of nodes (u1, v1) in G1 and (u2, v2) in G2 should
        be considered equal during matching.

        The function will be called like

           edge_match(G1[u1][v1], G2[u2][v2]).

        That is, the function will receive the edge attribute
        dictionaries of the edges under consideration.

        Ignored if edge_subst_cost is specified.  If neither
        edge_match nor edge_subst_cost are specified then edge
        attributes are not considered.

    node_subst_cost, node_del_cost, node_ins_cost : callable
        Functions that return the costs of node substitution, node
        deletion, and node insertion, respectively.

        The functions will be called like

           node_subst_cost(G1.nodes[n1], G2.nodes[n2]),
           node_del_cost(G1.nodes[n1]),
           node_ins_cost(G2.nodes[n2]).

        That is, the functions will receive the node attribute
        dictionaries as inputs.  The functions are expected to return
        positive numeric values.

        Function node_subst_cost overrides node_match if specified.
        If neither node_match nor node_subst_cost are specified then
        default node substitution cost of 0 is used (node attributes
        are not considered during matching).

        If node_del_cost is not specified then default node deletion
        cost of 1 is used.  If node_ins_cost is not specified then
        default node insertion cost of 1 is used.

    edge_subst_cost, edge_del_cost, edge_ins_cost : callable
        Functions that return the costs of edge substitution, edge
        deletion, and edge insertion, respectively.

        The functions will be called like

           edge_subst_cost(G1[u1][v1], G2[u2][v2]),
           edge_del_cost(G1[u1][v1]),
           edge_ins_cost(G2[u2][v2]).

        That is, the functions will receive the edge attribute
        dictionaries as inputs.  The functions are expected to return
        positive numeric values.

        Function edge_subst_cost overrides edge_match if specified.
        If neither edge_match nor edge_subst_cost are specified then
        default edge substitution cost of 0 is used (edge attributes
        are not considered during matching).

        If edge_del_cost is not specified then default edge deletion
        cost of 1 is used.  If edge_ins_cost is not specified then
        default edge insertion cost of 1 is used.

    upper_bound : numeric
        Maximum edit distance to consider.

    Returns
    -------
    Generator of consecutive approximations of graph edit distance.

    Examples
    --------
    >>> G1 = nx.cycle_graph(6)
    >>> G2 = nx.wheel_graph(7)
    >>> for v in nx.optimize_graph_edit_distance(G1, G2):
    ...     minv = v
    >>> minv
    7.0

    See Also
    --------
    graph_edit_distance, optimize_edit_paths

    References
    ----------
    .. [1] Zeina Abu-Aisheh, Romain Raveaux, Jean-Yves Ramel, Patrick
       Martineau. An Exact Graph Edit Distance Algorithm for Solving
       Pattern Recognition Problems. 4th International Conference on
       Pattern Recognition Applications and Methods 2015, Jan 2015,
       Lisbon, Portugal. 2015,
       <10.5220/0005209202710278>. <hal-01168816>
       https://hal.archives-ouvertes.fr/hal-01168816
    TN)r   )r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   D  s    ~		Tc          
   #   s<  d d l   d d l m  Gd d   d     f d d    d d	   
 d
 d    d d    g     
    f d d     f d d           f d d        f d d    t  j  } t  j   t |   t     j     f  }  r j     f d d   | D  j    | d   d   f <nV  râ j     f d d   | D  j    | d   d   f <n   r  f d d   | D  n d g t |    r?  f d d    D  n d g t    | d   d   f j	   t	   t	   d   j    f d d   t
   D  j    | d       f < j    f d d   t
   D  j    |     d   f < |     t  j  } t  j   t |   t     j     f  } 	 rï j   	  f d d   | D  j    | d   d   f <nV  rE j     f d d   | D  j    | d   d   f <n   rj  f d  d   | D  n d g t |    r¢  f d! d    D  n d g t    | d   d   f j	   t	   t	   d   j    f d" d   t
   D  j    | d       f < j    f d# d   t
   D  j    |     d   f < |      G   f d$ d%   d%  } |       ! f d& d'    xO  g  |   g  |    d 	 D]) \ } } } t |  t |  | f VqWd S)(a  GED (graph edit distance) calculation: advanced interface.

    Graph edit path is a sequence of node and edge edit operations
    transforming graph G1 to graph isomorphic to G2.  Edit operations
    include substitutions, deletions, and insertions.

    Graph edit distance is defined as minimum cost of edit path.

    Parameters
    ----------
    G1, G2: graphs
        The two graphs G1 and G2 must be of the same type.

    node_match : callable
        A function that returns True if node n1 in G1 and n2 in G2
        should be considered equal during matching.

        The function will be called like

           node_match(G1.nodes[n1], G2.nodes[n2]).

        That is, the function will receive the node attribute
        dictionaries for n1 and n2 as inputs.

        Ignored if node_subst_cost is specified.  If neither
        node_match nor node_subst_cost are specified then node
        attributes are not considered.

    edge_match : callable
        A function that returns True if the edge attribute dictionaries
        for the pair of nodes (u1, v1) in G1 and (u2, v2) in G2 should
        be considered equal during matching.

        The function will be called like

           edge_match(G1[u1][v1], G2[u2][v2]).

        That is, the function will receive the edge attribute
        dictionaries of the edges under consideration.

        Ignored if edge_subst_cost is specified.  If neither
        edge_match nor edge_subst_cost are specified then edge
        attributes are not considered.

    node_subst_cost, node_del_cost, node_ins_cost : callable
        Functions that return the costs of node substitution, node
        deletion, and node insertion, respectively.

        The functions will be called like

           node_subst_cost(G1.nodes[n1], G2.nodes[n2]),
           node_del_cost(G1.nodes[n1]),
           node_ins_cost(G2.nodes[n2]).

        That is, the functions will receive the node attribute
        dictionaries as inputs.  The functions are expected to return
        positive numeric values.

        Function node_subst_cost overrides node_match if specified.
        If neither node_match nor node_subst_cost are specified then
        default node substitution cost of 0 is used (node attributes
        are not considered during matching).

        If node_del_cost is not specified then default node deletion
        cost of 1 is used.  If node_ins_cost is not specified then
        default node insertion cost of 1 is used.

    edge_subst_cost, edge_del_cost, edge_ins_cost : callable
        Functions that return the costs of edge substitution, edge
        deletion, and edge insertion, respectively.

        The functions will be called like

           edge_subst_cost(G1[u1][v1], G2[u2][v2]),
           edge_del_cost(G1[u1][v1]),
           edge_ins_cost(G2[u2][v2]).

        That is, the functions will receive the edge attribute
        dictionaries as inputs.  The functions are expected to return
        positive numeric values.

        Function edge_subst_cost overrides edge_match if specified.
        If neither edge_match nor edge_subst_cost are specified then
        default edge substitution cost of 0 is used (edge attributes
        are not considered during matching).

        If edge_del_cost is not specified then default edge deletion
        cost of 1 is used.  If edge_ins_cost is not specified then
        default edge insertion cost of 1 is used.

    upper_bound : numeric
        Maximum edit distance to consider.

    strictly_decreasing : bool
        If True, return consecutive approximations of strictly
        decreasing cost.  Otherwise, return all edit paths of cost
        less than or equal to the previous minimum cost.

    Returns
    -------
    Generator of tuples (node_edit_path, edge_edit_path, cost)
        node_edit_path : list of tuples (u, v)
        edge_edit_path : list of tuples ((u1, v1), (u2, v2))
        cost : numeric

    See Also
    --------
    graph_edit_distance, optimize_graph_edit_distance, optimal_edit_paths

    References
    ----------
    .. [1] Zeina Abu-Aisheh, Romain Raveaux, Jean-Yves Ramel, Patrick
       Martineau. An Exact Graph Edit Distance Algorithm for Solving
       Pattern Recognition Problems. 4th International Conference on
       Pattern Recognition Applications and Methods 2015, Jan 2015,
       Lisbon, Portugal. 2015,
       <10.5220/0005209202710278>. <hal-01168816>
       https://hal.archives-ouvertes.fr/hal-01168816

    r   N)Úlinear_sum_assignmentc               @   s   e  Z d  Z d d   Z d S)z'optimize_edit_paths.<locals>.CostMatrixc             S   s(   | |  _  | |  _ | |  _ | |  _ d  S)N)ÚCÚlsa_row_indÚlsa_col_indÚls)Úselfr!   r"   r#   r$   r   r   r   Ú__init__N  s    			z0optimize_edit_paths.<locals>.CostMatrix.__init__N)Ú__name__Ú
__module__Ú__qualname__r&   r   r   r   r   Ú
CostMatrixM  s   r*   c                sÙ    |   \ } } t  t t |   | |  } t    f d d   | D  } t  t t |   | |  } t    f d d   | D  } | |   | | <| |  | | < |  | | |  | | f j    S)Nc             3   s6   |  ], \ } } } |   k  r |  k  r | Vq d  S)Nr   )Ú.0ÚkÚiÚj)ÚmÚnr   r   ú	<genexpr>c  s    z?optimize_edit_paths.<locals>.make_CostMatrix.<locals>.<genexpr>c             3   s6   |  ], \ } } } |   k r |  k r | Vq d  S)Nr   )r+   r,   r-   r.   )r/   r0   r   r   r1   e  s    )ÚzipÚrangeÚlenr   Úsum)r!   r/   r0   r"   r#   ÚindexesZ	subst_indZ	dummy_ind)r*   r    )r/   r0   r   Úmake_CostMatrixZ  s    ""z,optimize_edit_paths.<locals>.make_CostMatrixc                sv       f d d   t     D }     f d d   t     D } |  | d  d   f d  d   | f S)Nc                s,   g  |  ]" } |   k p% |   k  q Sr   r   )r+   r,   )r-   r.   r/   r   r   ú
<listcomp>o  s   	 z:optimize_edit_paths.<locals>.extract_C.<locals>.<listcomp>c                s,   g  |  ]" } |  k p% |    k  q Sr   r   )r+   r,   )r-   r.   r0   r   r   r8   p  s   	 )r3   )r!   r-   r.   r/   r0   Úrow_indÚcol_indr   )r-   r.   r/   r0   r   Ú	extract_Cm  s    ))z&optimize_edit_paths.<locals>.extract_Cc                sv       f d d   t     D }     f d d   t     D } |  | d  d   f d  d   | f S)Nc                s,   g  |  ]" } |   k o% |   k  q Sr   r   )r+   r,   )r-   r.   r/   r   r   r8   u  s   	 z9optimize_edit_paths.<locals>.reduce_C.<locals>.<listcomp>c                s,   g  |  ]" } |  k o% |    k  q Sr   r   )r+   r,   )r-   r.   r0   r   r   r8   v  s   	 )r3   )r!   r-   r.   r/   r0   r9   r:   r   )r-   r.   r/   r0   r   Úreduce_Cs  s    ))z%optimize_edit_paths.<locals>.reduce_Cc                sL   |    f d d   |  D } x( t     D] } | | | k d 8<q* W| S)Nc                s   g  |  ] } |   k  q Sr   r   )r+   r,   )r-   r   r   r8   {  s   	 z;optimize_edit_paths.<locals>.reduce_ind.<locals>.<listcomp>é   )Úset)Úindr-   Zrindr,   r   )r-   r   Ú
reduce_indy  s    z'optimize_edit_paths.<locals>.reduce_indc                sF  t  	    t  
    	  f d d   t    D   
  f d d   t   D  t     t      s  r | j       } x"t t     D]\ } } 	 | d d   xè t t     D]Ñ \ }	 }
 
 |
 d d   t j   s+t j   rYt     f d d    D  rqí n+ t     f d d    D  rqí    f k rqí    f k r®qí  | | |	 f <qí Wq· W |    } t        f d	 d   t | j | j	  D  } n$ g  }   j
 d  g  g  d
  } | | f S)a  
        Parameters:
            u, v: matched vertices, u=None or v=None for
               deletion/insertion
            pending_g, pending_h: lists of edges not yet mapped
            Ce: CostMatrix of pending edge mappings
            matched_uv: partial vertex edit path
                list of tuples (u, v) of previously matched vertex
                    mappings u<->v, u=None or v=None for
                    deletion/insertion

        Returns:
            list of (i, j): indices of edge mappings g<->h
            localCe: local CostMatrix of edge mappings
                (basically submatrix of Ce at cross of rows i, cols j)
        c                s[   g  |  ]Q      d  d    f k sQ t      f d d    D  r    q S)Né   c             3   sA   |  ]7 \ } }    d  d  |  f  | f f k Vq d  S)NrA   r   )r+   ÚpÚq)r-   Ú	pending_gÚur   r   r1     s   zFoptimize_edit_paths.<locals>.match_edges.<locals>.<listcomp>.<genexpr>)Úany)r+   )Ú
matched_uvrD   rE   )r-   r   r8     s   	 #z<optimize_edit_paths.<locals>.match_edges.<locals>.<listcomp>c                s[   g  |  ]Q      d  d    f k sQ t      f d d    D  r    q S)NrA   c             3   sA   |  ]7 \ } }    d  d  |  f  | f f k Vq d  S)NrA   r   )r+   rB   rC   )r.   Ú	pending_hÚvr   r   r1     s   zFoptimize_edit_paths.<locals>.match_edges.<locals>.<listcomp>.<genexpr>)rF   )r+   )rG   rH   rI   )r.   r   r8     s   	 #NrA   c             3   s]   |  ]S \ } }   |  f k r3  |  f k pT    | f k oT   | f k Vq d  S)Nr   )r+   rB   rC   )ÚgÚhrE   rI   r   r   r1   ©  s   z;optimize_edit_paths.<locals>.match_edges.<locals>.<genexpr>c             3   sQ   |  ]G \ } }   |  f  | f f k oH  |  f  | f f k Vq d  S)Nr   )r+   rB   rC   )rJ   rK   rE   rI   r   r   r1   ®  s   c             3   su   |  ]k \ } } |  k  s' |  k  r |  k  r=  | n    | |  k  r^  | n   | f Vq d  S)Nr   )r+   r,   Úl)ÚMÚNÚg_indÚh_indr/   r0   r   r   r1   ¸  s   	r   )r   r   )r4   r3   r!   r2   ÚnxZis_directedrF   r   r"   r#   Úempty)rE   rI   rD   rH   ÚCerG   r!   r,   r-   rL   r.   ÚlocalCeÚij)r*   r   r   r;   Úinfr7   Únp)rM   rN   rJ   rO   rK   rP   r/   rG   r0   rD   rH   rE   rI   r   Úmatch_edges  s<    %%""!z(optimize_edit_paths.<locals>.match_edgesc                s   t  |  r t |   \ } }   t   f d d   | D  }  t  f d d   | D  }   |  j | |     | |  S|  Sd  S)Nc             3   s!   |  ] } |   k  r d  Vq d S)r=   Nr   )r+   Út)r/   r   r   r1   Æ  s    z9optimize_edit_paths.<locals>.reduce_Ce.<locals>.<genexpr>c             3   s!   |  ] } |   k  r d  Vq d S)r=   Nr   )r+   rY   )r0   r   r   r1   Ç  s    )r4   r2   r5   r!   )rS   rU   r/   r0   r-   r.   Zm_iZn_j)r7   r<   )r/   r0   r   Ú	reduce_CeÃ  s    ##%z&optimize_edit_paths.<locals>.reduce_Cec          	   3   sî  t  |   t  |   t   f d d   t | j | j  D  \ } }	  |  k  rh | | n d |	  k  r | |	 n d | | | |   \ }
 } 	 | |
 t  |  t  |   }  | | j | j | j  rá n   | j | f |	 f    
 | j |  |	 f  
 | j |	  | f  | j | j | |	 f  } | |	 f | |
 | | j | |	 f | j f Vt   } | |	      k rÍ    f d d   t    D } n)     f d d   t    D } xÉ| D]Á\ } }	  | | j | |	 f | j  r0qý  | j | f |	 f    |  k  rg d n  |	  k  r d n   }  | | j | |	 f | j | j  r·qý |  k  rÐ| | n d |	  k  ré| |	 n d | | | |   \ }
 }  | | j | |	 f | j | j  r2qý	 | |
 t  |  t  |   }  | | j | |	 f | j | j | j  rqý| j	 | |	 f | |
 | | j | |	 f | j f  qýWx% t
 | d d d	   D] } | VqÛWd S)
aê  
        Parameters:
            matched_uv: partial vertex edit path
                list of tuples (u, v) of vertex mappings u<->v,
                u=None or v=None for deletion/insertion
            pending_u, pending_v: lists of vertices not yet mapped
            Cv: CostMatrix of pending vertex mappings
            pending_g, pending_h: lists of edges not yet mapped
            Ce: CostMatrix of pending edge mappings
            matched_cost: cost of partial edit path

        Returns:
            sequence of
                (i, j): indices of vertex mapping u<->v
                Cv_ij: reduced CostMatrix of pending vertex mappings
                    (basically Cv with row i, col j removed)
                list of (x, y): indices of edge mappings g<->h
                Ce_xy: reduced CostMatrix of pending edge mappings
                    (basically Ce with rows x, cols y removed)
                cost: total cost of edit operation
            NOTE: most promising ops first
        c             3   s9   |  ]/ \ } } |   k  s' |  k  r | | f Vq d  S)Nr   )r+   r,   rL   )r/   r0   r   r   r1   é  s    	z<optimize_edit_paths.<locals>.get_edit_ops.<locals>.<genexpr>Nc             3   sC   |  ]9 } |   k r |  k  s1 |   k r |  f Vq d  S)Nr   )r+   rY   )Úfixed_iÚfixed_jr/   r   r   r1   þ  s    c             3   sC   |  ]9 } |  k r |  k  s1 |    k r   | f Vq d  S)Nr   )r+   rY   )r[   r\   r0   r   r   r1     s    r=   Úkeyc             S   s   |  d |  d j  |  d j  S)Né   r=   é   )r$   )rY   r   r   r   Ú<lambda>  s    z;optimize_edit_paths.<locals>.get_edit_ops.<locals>.<lambda>)r4   Úminr2   r"   r#   r$   r!   r   r3   r   Úsorted)rG   Ú	pending_uÚ	pending_vÚCvrD   rH   rS   Úmatched_costr-   r.   ÚxyrT   ÚCe_xyÚCv_ijÚotherÚ
candidatesrY   )r*   r7   rX   Úpruner<   rZ   r@   )r[   r\   r/   r0   r   Úget_edit_opsÌ  sN    7!!!.	,)$!++!%:z)optimize_edit_paths.<locals>.get_edit_opsc	             3   sA   | | j  | j   r d St t |  t |   s_ t  j |   _ |  | | f VnÞ |  | | |    | |  }	 xº|	 D]²\ }
 } } } } |
 \ } }  | | | j  | j   rÉ q | t |  k  rê | j |  n d } | t |  k  r| j |  n d } |  j | | f  xk | D]c \ } } t    } t   } | j | | k  rq  | n d | | k  r | n d f  q1Wt t d d   | D   } t t d d   | D   } t   f d d   t	 |  D  } t  f d d   t	 |  D  } x5  |  | | | |    | | | 	 D] } | VqIW| d k	 rt| j
 | |  | d k	 r| j
 | |  |  j   x? t | t	 |   D]( \ } } | d k	 r°  j
 | |  q°Wx? t | t	 |   D]( \ } } | d k	 rò j
 | |  qòWx | D] } | j   q%Wq Wd S)a  
        Parameters:
            matched_uv: partial vertex edit path
                list of tuples (u, v) of vertex mappings u<->v,
                u=None or v=None for deletion/insertion
            pending_u, pending_v: lists of vertices not yet mapped
            Cv: CostMatrix of pending vertex mappings
            matched_gh: partial edge edit path
                list of tuples (g, h) of edge mappings g<->h,
                g=None or h=None for deletion/insertion
            pending_g, pending_h: lists of edges not yet mapped
            Ce: CostMatrix of pending edge mappings
            matched_cost: cost of partial edit path

        Returns:
            sequence of (vertex_path, edge_path, cost)
                vertex_path: complete vertex edit path
                    list of tuples (u, v) of vertex mappings u<->v,
                    u=None or v=None for deletion/insertion
                edge_path: complete edge edit path
                    list of tuples (g, h) of edge mappings g<->h,
                    g=None or h=None for deletion/insertion
                cost: total cost of edit path
            NOTE: path costs are non-increasing
        Nc             s   s   |  ] \ } } | Vq d  S)Nr   )r+   ÚxÚyr   r   r   r1   c  s    z>optimize_edit_paths.<locals>.get_edit_paths.<locals>.<genexpr>c             s   s   |  ] \ } } | Vq d  S)Nr   )r+   rn   ro   r   r   r   r1   d  s    c             3   s6   |  ], } | t     k  r*   j |  n d  Vq d  S)N)r4   Úpop)r+   rn   )rD   r   r   r1   e  s   c             3   s6   |  ], } | t     k  r*   j |  n d  Vq d  S)N)r4   rp   )r+   ro   )rH   r   r   r1   g  s   )r$   Úmaxr4   ra   Úvaluerp   r   r   rb   ÚreversedÚinsertr2   )rG   rc   rd   re   Z
matched_ghrD   rH   rS   rf   Zedit_opsrU   ri   rg   rh   Z	edit_costr-   r.   rE   rI   rn   ro   Zlen_gZlen_hZsortedxZsortedyÚGÚHrY   rJ   rK   )rm   Úget_edit_pathsÚmaxcostrl   )rD   rH   r   rw     sX    *''$		
""z+optimize_edit_paths.<locals>.get_edit_pathsc                s:   g  |  ]0 }  D]# }    j  |  j  |   q q Sr   )Únodes)r+   rE   rI   )r   r   r   rd   r   r   r8     s   	z'optimize_edit_paths.<locals>.<listcomp>c          	      sD   g  |  ]: }  D]- } d  t     j |  j |    q q S)r=   )Úintry   )r+   rE   rI   )r   r   r   rd   r   r   r8     s   	c                s#   g  |  ] }    j  |   q Sr   )ry   )r+   rE   )r   r   r   r   r8     s   	 r=   c                s#   g  |  ] }    j  |   q Sr   )ry   )r+   rI   )r   r   r   r   r8     s   	 c                s?   g  |  ]5 } t    D]" } | | k r2   | n   q q Sr   )r3   )r+   r-   r.   )Ú	del_costsrV   r/   r   r   r8   ¡  s   	c                s?   g  |  ]5 } t    D]" } | | k r2  | n    q q Sr   )r3   )r+   r-   r.   )rV   Ú	ins_costsr0   r   r   r8   ¤  s   	c                s:   g  |  ]0 }  D]# }    j  |  j  |   q q Sr   )Úedges)r+   rJ   rK   )r   r   r   rH   r   r   r8   ³  s   	c          	      sD   g  |  ]: }  D]- } d  t     j |  j |    q q S)r=   )rz   r}   )r+   rJ   rK   )r   r   r   rH   r   r   r8   ·  s   	c                s#   g  |  ] }    j  |   q Sr   )r}   )r+   rJ   )r   r   r   r   r8   ¿  s   	 c                s#   g  |  ] }    j  |   q Sr   )r}   )r+   rK   )r   r   r   r   r8   Ä  s   	 c                s?   g  |  ]5 } t    D]" } | | k r2   | n   q q Sr   )r3   )r+   r-   r.   )r{   rV   r/   r   r   r8   É  s   	c                s?   g  |  ]5 } t    D]" } | | k r2  | n    q q Sr   )r3   )r+   r-   r.   )rV   r|   r0   r   r   r8   Ì  s   	c                   s%   e  Z d  Z    f d d   Z d S)z$optimize_edit_paths.<locals>.MaxCostc                s'    j  j     j  j   d |  _ d  S)Nr=   )r!   r5   rr   )r%   )rS   re   r   r   r&   Õ  s    z-optimize_edit_paths.<locals>.MaxCost.__init__N)r'   r(   r)   r&   r   )rS   re   r   r   ÚMaxCostÔ  s   r~   c                sL    d  k	 r |   k r d S|    j  k r/ d S rH |    j  k rH d Sd  S)NT)rr   )r   )rx   Ústrictly_decreasingr   r   r   rl   Û  s    z"optimize_edit_paths.<locals>.prune)ÚnumpyZscipy.optimizer    r   ry   r4   ÚzerosÚarrayZreshaper5   r3   r}   )r   r   r   r   r   r   r   r   r   r   r   r   rc   r!   rD   r~   r   r   r   r   )"rS   r*   re   r   r   r{   r   r   r   r   r;   rm   rw   rV   r|   r    r/   r7   rX   rx   r0   r   r   r   r   rW   rH   rd   rl   r<   rZ   r@   r   r   r   r   Ê  s    'C	$Pg%%:&&%%:&&	"c             C   sb   d d l  m } y d d l } Wn | d   Yn Xy d d l } Wn | d   Yn Xd S)zFixture for nose tests.r   )ÚSkipTestNzNumPy not availablezSciPy not available)Znoser   r   Úscipy)Úmoduler   r   r   r   r   r   Úsetup_moduleò  s    r   )Ú__doc__Ú
__future__r   ÚmathZnetworkxrQ   ÚoperatorÚsysÚ
__author__Ú__all__r   r   r   r   r   r   r   r   r   r   Ú<module>   sL   
	ÿ ÿ %